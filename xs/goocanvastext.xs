#include "goocanvas-perl.h"


MODULE = Goo::Canvas::Text		PACKAGE = Goo::Canvas::Text   PREFIX = goo_canvas_text_

GooCanvasItem*
goo_canvas_text_new(class, parent, string, x, y, width, anchor, ...)
    GooCanvasItem *parent
    const char *string
    gdouble x
    gdouble y
    gdouble width
    GtkAnchorType anchor
   CODE:
    RETVAL = goo_canvas_text_new(parent, string, x, y, width, anchor, NULL);
    GOOCANVAS_PERL_ADD_PROPETIES(7);
   OUTPUT:
    RETVAL

=for apidoc
Gets the natural extents of the text, in the text item's coordinate space.

The final extents of the text may be different if the text item is placed in a layout container such as GooCanvasTable.
=cut
## call as ($ink_rect, $logical_rect) = $text->get_natural_extents
void
goo_canvas_text_get_natural_extents(text)
    GooCanvasText  *text
   PREINIT:
    PangoRectangle ink_rect;
    PangoRectangle logical_rect;
   PPCODE:
    goo_canvas_text_get_natural_extents (text, &ink_rect, &logical_rect);
    XPUSHs(sv_2mortal(newSVPangoRectangle(&ink_rect)));
    XPUSHs(sv_2mortal(newSVPangoRectangle(&logical_rect)));


MODULE = Goo::Canvas::Text		PACKAGE = Goo::Canvas::TextModel   PREFIX = goo_canvas_text_model_

GooCanvasItemModel*
goo_canvas_text_model_new(class, parent, string, x, y, width, anchor, ...)
    GooCanvasItemModel *parent
    const char *string
    gdouble x
    gdouble y
    gdouble width
    GtkAnchorType anchor
   CODE:
    RETVAL = goo_canvas_text_model_new(parent, string, x, y, width, anchor, NULL);
    GOOCANVAS_PERL_ADD_PROPETIES(7);
   OUTPUT:
    RETVAL
